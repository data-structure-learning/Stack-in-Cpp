//
//  Stack.hpp
//  Stack-in-Cpp
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#ifndef Stack_hpp
#define Stack_hpp

#include <stdio.h>

using namespace std;

template <typename T>
class Stack {
    
public:
    Stack(int stackCapacity);
    ~Stack();
    void ClearStack();
    bool StackEmpty();
    bool StackFull();
    int StackLength();
    bool Push(T element);
    bool Pop(T &element);
    void StackTraverse();
    
private:
    T *m_pSatck;
    int m_iSatckCapacity;
    
    int m_iTop;
};

template <typename T>
Stack<T>::Stack(int stackCapacity) {
    m_iSatckCapacity = stackCapacity;
    m_pSatck = new T[m_iSatckCapacity];
    ClearStack();
}

template <typename T>
Stack<T>::~Stack() {
    delete [] m_pSatck;
    m_pSatck = NULL;
}

template <typename T>
void Stack<T>::ClearStack() {
    m_iTop = 0;
}

template <typename T>
bool Stack<T>::StackEmpty() {
    return m_iTop == 0;
}

template <typename T>
bool Stack<T>::StackFull() {
    return m_iTop >= m_iSatckCapacity;
}

template <typename T>
int Stack<T>::StackLength() {
    return m_iTop;
}

template <typename T>
bool Stack<T>::Push(T element) {
    if (StackFull()) {
        return false;
    }
    
    m_pSatck[m_iTop ++] = element;
    return true;
}

template <typename T>
bool Stack<T>::Pop(T &element) {
    if (StackEmpty()) {
        return false;
    }
    
    element = m_pSatck[-- m_iTop];
    return true;
}

template <typename T>
void Stack<T>::StackTraverse() {
    for (int i = m_iTop - 1; i >= 0; i --) {
        cout << m_pSatck[i] << " ";
    }
    
    cout << endl;
}

#endif /* Stack_hpp */
