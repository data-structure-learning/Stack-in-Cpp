//
//  main.cpp
//  Stack-in-Cpp
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#include <iostream>

#include "Stack.hpp"
#include "Coordinate.hpp"

using namespace std;

void testIntStack();
void testObjectStack();

int main(int argc, const char * argv[]) {
    
    cout << "testIntStack" << endl;
    testIntStack();
    
    cout << "testObjectStack" << endl;
    testObjectStack();
    
    return 0;
}

void testIntStack() {
    Stack<int> *s = new Stack<int>(5);
    
    s->Push(2);
    s->Push(5);
    s->Push(7);
    
    s->StackTraverse();
    
    int e = 0;
    s->Pop(e);
    cout << "e: " << e << endl;
    
    s->StackTraverse();
    
    s->Push(12);
    s->Push(19);
    s->Push(31);
    s->Push(50);
    
    s->StackTraverse();
    
    
    s->Pop(e);
    s->Pop(e);
    s->Pop(e);
    s->Pop(e);
    s->Pop(e);
    s->Pop(e);
    
    s->StackTraverse();
    
    s->Push(81);
    s->ClearStack();
    
    s->StackTraverse();
    
    delete s;
    s = NULL;
}

void testObjectStack() {
    Stack<Coordinate> *s = new Stack<Coordinate>(5);
    
    s->Push(Coordinate(2, 2));
    s->Push(Coordinate(5, 5));
    s->Push(Coordinate(7, 7));
    
    s->StackTraverse();
    
    Coordinate *c = new Coordinate();
    s->Pop(*c);
    cout << "c: " << *c << endl;
    
    s->StackTraverse();
    
    s->Push(Coordinate(12, 12));
    s->Push(Coordinate(19, 19));
    s->Push(Coordinate(31, 31));
    s->Push(Coordinate(50, 50));
    
    s->StackTraverse();
    
    s->Pop(*c);
    s->Pop(*c);
    s->Pop(*c);
    s->Pop(*c);
    s->Pop(*c);
    s->Pop(*c);
    
    s->StackTraverse();
    
    s->Push(81);
    s->ClearStack();
    
    s->StackTraverse();
}
