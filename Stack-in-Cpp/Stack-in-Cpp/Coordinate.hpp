//
//  Coordinate.hpp
//  Stack-in-Cpp
//
//  Created by 买明 on 21/09/2016.
//  Copyright © 2016 买明. All rights reserved.
//

#ifndef Coordinate_hpp
#define Coordinate_hpp

#include <stdio.h>

#include <ostream>

using namespace std;

class Coordinate {
    // 重载 << 运算符
    friend ostream &operator<<(ostream &out, Coordinate &c);
    
public:
    Coordinate(int x = 0, int y = 0);
    
private:
    int m_iX;
    int m_iY;
};

Coordinate::Coordinate(int x, int y) {
    m_iX = x;
    m_iY = y;
}

ostream &operator<<(ostream &out, Coordinate &c) {
    
    out << "(" << c.m_iX << ", " << c.m_iY << ")";
    return out;
}


#endif /* Coordinate_hpp */
